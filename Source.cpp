#include <iostream>
#include <list>
#include <vector>
#include "graph.h"
using namespace std;

int main()
{
	int **arr, weight, node, i, j;
	Graph graph;

	cout << "How many node(s) do you want: ";
	cin >> node;
	cout << "---------------------------------------------------------" << endl;
	//creat adjacency matrix
	arr = new int* [node];
	for (i = 0; i < node; i++)
	{
		arr[i] = new int[node];
		for (j = 0; j < node; j++)
		{
			arr[i][j] = 0; //initialize the matrix
		}
	}

	/*arr[0][0] = 1;
	arr[0][1] = 2;

	arr[1][0] = 2;
	arr[1][1] = 1;*/

	for (i = 0; i < node; i++)
	{
		for ( j = 0; j < node; j++)
		{
			cout << "Input weight for arr[" << i << "][" << j << "] --> ";
			cin >> weight; //input from user
			arr[i][j] = weight;
		}
	}

	cout << "---------------------------------------------------------" << endl;
	for (i = 0; i < node; i++)
	{
		for (j = 0; j < node; j++)
		{
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}

	graph.arrayToList(arr, node);
	graph.printGraph();

	cout << "**CHECK THE GRAPH**" << endl;

	if (graph.multiGraph() == true)
	{
		cout << "This graph is **Multigraph**" << endl;
	}
	else
	{
		cout << "This graph is not **Multigraph**" << endl;
	}

	if (graph.pseudoGraph() == true)
	{
		cout << "This graph is **Pseudograh**" << endl;
	}
	else
	{
		cout << "This graph is not **Pseudograph**" << endl;
	}

	if (graph.digraph() == true)
	{
		cout << "This graph is **Digraph**" << endl;
	}
	else
	{
		cout << "This graph is not **Digraph**" << endl;
	}


	if (graph.weightGraph() == true)
	{
		cout << "This graph is **Weightgraph**" << endl;
	}
	else
	{
		cout << "This graph is not **Weightgraph**" << endl;
	}


	if (graph.completeGraph() == true)
	{
		cout << "This graph is **completegraph**" << endl;
	}
	else
	{
		cout << "This graph is not **completegraph**" << endl;
	}
	cout << "---------------------------------------------------------" << endl;
	cout << endl << "**DYKSTRA [Shortest Path]**" << endl;
	graph.dykstra('A');

}